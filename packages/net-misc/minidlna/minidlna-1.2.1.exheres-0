# Copyright 2018 Thomas Witt
# Distributed under the terms of the GNU General Public License v2
# systemd integration copied from Archlinux:
# https://www.archlinux.org/packages/community/x86_64/minidlna/

require sourceforge [ suffix=tar.gz ] \
    systemd-service [ systemd_tmpfiles=[ "${FILES}"/tmpfiles.d ] ]

SUMMARY="A simple media server software"
DESCRIPTION="
The MiniDLNA daemon is an UPnP-A/V and DLNA service which serves multimedia
content to compatible clients on the network.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-db/sqlite:3
        media-libs/flac
        media-libs/libexif
        media-libs/libid3tag
        media-libs/libogg
        media-libs/libvorbis
        providers:ffmpeg? ( media/ffmpeg )
        providers:libav? ( media/libav )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    run:
        group/${PN}
        user/${PN}
"

BUGS_TO="pyromaniac@exherbo.org"

src_install() {
    default

    install_systemd_files

    doman ${PN}.conf.5
    doman ${PN}d.8
}

