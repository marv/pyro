Title: owncloud client rename
Author: Thomas Witt <pyromaniac@exherbo.org>
Content-Type: text/plain
Posted: 2015-05-13
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: net-misc/mirall

The owncloud client software has been renamed from “mirall” to “owncloudclient”
by upstream. Please uninstall net-misc/mirall and use net-misc/owncloudclient
instead.
